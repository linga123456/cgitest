import { RequestCountriesEffect } from "./request-countries.effect";
import { ReplaySubject, of } from 'rxjs';
import { TestBed, async } from '@angular/core/testing';
import { ApiService } from '../services/api.service';
import { OptionsActions } from '../actions/options.actions';
import { IBaseAction } from '../interfaces/base.action';
import { createAction } from '../utils/create-action.util';
import { StoreModule } from '@ngrx/store';
import reducers from '../reducers';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';


describe('RequestSiteTypesEffect', () => {
  let effects: RequestCountriesEffect;
  let actions: ReplaySubject<any>;


  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RequestCountriesEffect,
        ApiService,
      ],
      imports: [StoreModule.forRoot(
        reducers,
        {},
      ),
      EffectsModule.forRoot([RequestCountriesEffect]),
        HttpClientModule
      ]
    })
    effects = TestBed.get(RequestCountriesEffect);
  });

  it('should run on countries request and pass api requested countries into changed actions', async(() => {
    actions = new ReplaySubject(1);
    actions.next(createAction(OptionsActions.COUNTRIES_REQUESTED, 'europe'));

    effects.loadCountries$.subscribe((action: IBaseAction) => {
      expect(action.type).toEqual(OptionsActions.COUNTRIES_REQUESTED_SUCCEEDED);
      expect(action.payload).toEqual('europe');
    });
  }));
});
