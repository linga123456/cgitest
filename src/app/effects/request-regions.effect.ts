import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {OptionsActions} from '../actions/options.actions';
import {createAction} from '../utils/create-action.util';
import {ApiService} from '../services/api.service';


@Injectable({
  providedIn: 'root',
})
export class RequestRegionsEffect {

  loadRegions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(OptionsActions.REGIONS_REQUESTED),
      mergeMap(() => this.apiService.requestRegions().pipe(
        map((result: Observable<any>) => createAction(OptionsActions.REGIONS_REQUESTED_SUCCEEDED, result)),
        catchError((error: ErrorEvent) => of(createAction(OptionsActions.REGIONS_REQUESTED_FAILED, {error}))))),
    )
  );

  constructor(
    private actions$: Actions,
    private apiService: ApiService,
  ) {
  }
}
