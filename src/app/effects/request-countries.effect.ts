import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {IBaseAction} from '../interfaces/base.action';
import {OptionsActions} from '../actions/options.actions';
import {createAction} from '../utils/create-action.util';
import {ApiService} from '../services/api.service';


@Injectable({
  providedIn: 'root',
})
export class RequestCountriesEffect {

  loadCountries$ = createEffect(() =>
    this.actions$.pipe(
      ofType(OptionsActions.COUNTRIES_REQUESTED),
      mergeMap((action: IBaseAction) => this.apiService.requestCountries(action.payload).pipe(
        map((result: Observable<any>) => createAction(OptionsActions.COUNTRIES_REQUESTED_SUCCEEDED, result)),
        catchError((error: ErrorEvent) => of(createAction(OptionsActions.COUNTRIES_REQUESTED_FAILED, {error}))))),
    )
  );

  constructor(
    private actions$: Actions,
    private apiService: ApiService,
  ) {
  }
}
