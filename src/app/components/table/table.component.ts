import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent {
  @Input() selectedCountries: any;

  getFormattedCurrecies(currencies) {
    return currencies.map(currency => currency['name']).join(',');
  }

}
