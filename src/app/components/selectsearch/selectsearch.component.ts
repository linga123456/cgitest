import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {merge, Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map} from 'rxjs/operators';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-selectsearch',
  templateUrl: './selectsearch.component.html',
  styleUrls: ['./selectsearch.component.scss']
})
export class SelectsearchComponent {
  @Input() options: any;
  @Input() optionTitle: string;
  @Input() disabled: boolean;
  @Output() selectOptionEvent = new EventEmitter();

  @ViewChild('instance', {static: true}) instance: NgbTypeahead;
  @ViewChild('elem', {static: true}) elem: ElementRef;

  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  formatter = (x: { name: string }) => x.name;


  search = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.options
        : this.options.filter(option => option.name.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  };

  selected($event) {
    event.preventDefault();
    this.selectOptionEvent.emit($event);
  }


}
