import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { SelectsearchComponent } from './selectsearch.component';
import { NgbTypeahead, NgbTypeaheadModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Observable, interval } from 'rxjs';
import { take, map } from 'rxjs/operators';

describe('SelectsearchComponent', () => {
  let component: SelectsearchComponent;
  let fixture: ComponentFixture<SelectsearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectsearchComponent ],
      imports: [NgbModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectsearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
