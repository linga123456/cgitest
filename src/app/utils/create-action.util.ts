import {IBaseAction} from '../interfaces/base.action';

export function createAction(type: string, payload?: any, meta?: any): IBaseAction {
  return {type, payload, meta};
}
