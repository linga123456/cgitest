import {IOptions} from '../interfaces/options.interface';

export function createDefaultOptions(): IOptions {
  return {
    regions: {
      isLoading: false,
      error: null,
      types: [],
    },
    countries: {
      isLoading: false,
      error: null,
      types: [],
    },
  };
}

