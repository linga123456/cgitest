import {IOptions} from '../interfaces/options.interface';
import {createDefaultOptions} from '../utils/options.util';
import {IBaseAction} from '../interfaces/base.action';
import {OptionsActions} from '../actions/options.actions';

const optionsReducer = (
  state: IOptions = createDefaultOptions(),
  action: IBaseAction,
) => {
  switch (action.type) {
    case OptionsActions.COUNTRIES_REQUESTED:
      return {
        ...state,
        countries: {
          ...state.countries,
          isLoading: true,
        },
      };
    case OptionsActions.COUNTRIES_REQUESTED_SUCCEEDED:
      return {
        ...state,
        countries: {
          ...state.countries,
          isLoading: false,
          error: null,
          types: action.payload,
        },
      };
    case OptionsActions.COUNTRIES_REQUESTED_FAILED:
      return {
        ...state,
        countries: {
          ...state.countries,
          isLoading: false,
          error: action.payload.error,
        },
      };
    case OptionsActions.REGIONS_REQUESTED:
      return {
        ...state,
        regions: {
          ...state.regions,
          isLoading: true,
        },
      };
    case OptionsActions.REGIONS_REQUESTED_SUCCEEDED:
      return {
        ...state,
        regions: {
          ...state.regions,
          isLoading: false,
          types: action.payload,
          error: null,
        },
      };
    case OptionsActions.REGIONS_REQUESTED_FAILED:
      return {
        ...state,
        regions: {
          ...state.regions,
          isLoading: false,
          error: action.payload.error,
        },
      };
    default:
      return state;
  }
};

export default optionsReducer;
