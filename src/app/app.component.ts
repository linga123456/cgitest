import {Component, OnInit} from '@angular/core';
import {OptionsSelectors} from './selectors/options.selectors';
import {OptionsActions} from './actions/options.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  regionTitle = 'Select Region:';
  countryTitle = 'Select Country:';

  disabled = true;
  selectedCountries$;
  listofRegions$ = this.optionsSelectors.getRegions();
  listofCountries$ = this.optionsSelectors.getCountries();

  constructor(private optionsSelectors: OptionsSelectors,
              public optionsActions: OptionsActions) {
  }

  ngOnInit() {
    this.initializeData();
  }

  initializeData() {
    this.optionsActions.initialLoadOptions();
  }

  onCountrySelect(event) {
    this.selectedCountries$ = this.optionsSelectors.getSelectedCountries(event.item.name);
  }

  onRegionSelect(event) {
    const region = event.item.name;
    if (region != '') {
      this.disabled = false;
    } else {
      this.disabled = true;
    }
    this.optionsActions.requestCountries(region);
  }

}
