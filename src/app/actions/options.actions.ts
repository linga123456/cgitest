import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {createAction} from '../utils/create-action.util';

@Injectable({
  providedIn: 'root',
})
export class OptionsActions {
  static COUNTRIES_REQUESTED = 'COUNTRIES_REQUESTED';
  static COUNTRIES_REQUESTED_SUCCEEDED = 'COUNTRIES_REQUESTED_SUCCEEDED';
  static COUNTRIES_REQUESTED_FAILED = 'COUNTRIES_REQUESTED_FAILED';
  static REGIONS_REQUESTED = 'REGIONS_REQUESTED';
  static REGIONS_REQUESTED_FAILED = 'REGIONS_REQUESTED_FAILED';
  static REGIONS_REQUESTED_SUCCEEDED = 'REGIONS_REQUESTED_SUCCEEDED';

  constructor(private store: Store<any>) {
  }

  public initialLoadOptions() {
    this.requestRegions();
  }

  public requestCountries(region: string) {
    this.store.dispatch(createAction(OptionsActions.COUNTRIES_REQUESTED, region));
  }

  private requestRegions() {
    this.store.dispatch(createAction(OptionsActions.REGIONS_REQUESTED));
  }
}
