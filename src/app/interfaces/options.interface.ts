import {IRegions} from './region-types.interface';
import {ICountries} from './country-types.interface';

export interface IOptions {
  readonly regions: IRegions;
  readonly countries: ICountries;
}
