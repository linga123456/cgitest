import {ILoadable} from './loadable.interface';

export interface ICountries extends ILoadable {
  readonly types: Array<any>;
}
