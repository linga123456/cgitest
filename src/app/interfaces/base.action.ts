import {Action} from '@ngrx/store';

export interface IBaseAction extends Action {
  type: string;
  payload?: any;
  meta?: { requestType?: string, type?: string, root?: Partial<IBaseAction> };
  error?: any;
}
