import {ILoadable} from './loadable.interface';

export interface IRegions extends ILoadable {
  readonly types: Array<any>;
}
