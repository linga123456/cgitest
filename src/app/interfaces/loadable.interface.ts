export interface ILoadable {
  readonly isLoading: boolean;
  readonly error: any;
}
