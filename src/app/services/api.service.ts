import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root',
})
export class ApiService {

  constructor(private http: HttpClient) {
  }

  requestCountries(region: string): Observable<any> {
    return this.http.get(`${environment.apiUrl}${region}`);
  }

  requestRegions(): Observable<any> {
    return of([{id: '001', name: 'europe'}, {id: '002', name: 'asia'}]);
  }
}
