import {Injectable} from '@angular/core';
import {createSelector, Store} from '@ngrx/store';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class OptionsSelectors {

  static selectors = {
    getRegions: state => state.options.regions.types,
    getCountries: state => state.options.countries.types,
  };


  constructor(private store: Store<any>) {
  }

  getRegions() {
    return this.store.select(state => state.options.regions.types);
  }

  getCountries() {
    return this.store.select(state => state.options.countries.types);
  }

  getSelectedCountries(selectedCountry: string): Observable<Array<any>> {
    return this.store.select(createSelector(
      OptionsSelectors.selectors.getCountries,
      (countries) => {
        return countries.filter(e => e.name === selectedCountry);
      }));
  }
}
