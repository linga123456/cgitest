import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {StoreModule} from '@ngrx/store';
import reducers from './reducers';
import {environment} from '../environments/environment';
import {EffectsModule} from '@ngrx/effects';
import {NgbModule, NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {RequestRegionsEffect} from './effects/request-regions.effect';
import {RequestCountriesEffect} from './effects/request-countries.effect';
import {SelectsearchComponent} from './components/selectsearch/selectsearch.component';
import {TableComponent} from './components/table/table.component';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';

const metaReducers = environment.production ? [] : [];

@NgModule({
  declarations: [
    AppComponent,
    SelectsearchComponent,
    TableComponent
  ],
  imports: [
    BrowserModule,
    EffectsModule.forRoot([RequestRegionsEffect, RequestCountriesEffect]),
    HttpClientModule,
    NgbModule,
    NgbTypeaheadModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
      }
    }),
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
